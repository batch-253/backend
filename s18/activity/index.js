
//======= ADD

function addNum(num1, num2) {
	let sum = num1 + num2;
	console.log("Displayed sum of " + num1 + " " + num2 + "\n" + sum);
}

addNum(10,10);


//======= SUBTRACT

function subNum(num1, num2) {
	let sub = num1 - num2;
	console.log("Displayed difference of " + num1 + " " + num2 + "\n" + sub);
}

subNum(30,15);


//======= MULTIPLY


function multiplyNum(num1, num2) {
	let product = num1 * num2;

	return "The product of " + num1 + " " + "and" + " " + num2 + ":" + "\n" + product;
	

}

let result = multiplyNum(100,5);
console.log(result);


//======== DIVIDE


function divideNum(num1, num2) {
	let quotient = num1 / num2;
	return  "The quotient of " + num1 + " " + "and" + " " + num2 + ":" + "\n" + quotient;
}

let result2 = divideNum(50,5);
console.log(result2);


//====== GET CIRCLE AREA

function getCircleArea(radius) {
  const circleArea = Math.PI * Math.pow(radius, 2);

  return "The result of getting the area of a circle with " + radius + " " + "radius" + "\n" + circleArea.toFixed(2);
}

let result3 = getCircleArea(10);
console.log(result3);


//======= GET AVERAGE


function getAverage(num1, num2, num3, num4) {
  let averageVar = (num1 + num2 + num3 + num4) / 4;
  return "The average of " + num1 + " " + num2 + " " + num3 + " "+ "and" + " " + num4 + "\n" + averageVar;
}

let result4 = getAverage(20, 40, 60, 80);
console.log(result4); 



//======== Get PASSING SCORE

function checkIfPassed(score, totalScore) {
  let isPassingScore = (score / totalScore) * 100;
  let isPassed = isPassingScore >= 75;
  return "Is" + " " + score + "/" + totalScore + " " + "a passing score?" + "\n" + isPassed;
}

let result5 = checkIfPassed(38, 50);
console.log(result5);



//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}