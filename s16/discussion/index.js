console.log("Hello!")



// [SECTION] Arithmetic Operators

let x = 81, y = 9;

let sum = x + y;
console.log("Result of addition operator: " + sum);


let difference = x - y;
console.log("Result of substraction operator: " + difference);


let product = x * y;
console.log("Result of multiplacation operator: " + product);


let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = x % y;
console.log("Result of modulo operator: " + remainder);


// [SECTION] Assignment Operators
// Basic Assignment Operator (=)
// The assignment operator assigns the value of the **right** operand to a variable.

let assignmentNumber = 8;

// Addition Assignment operator
// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

// assignmentNumber = assignmentNumber+2;

assignmentNumber +=2;
 // Shorthand
console.log("Result of addition assignment operator: " + assignmentNumber);

assignmentNumber -=2;
console.log("Result of substraction assignment operator: " + assignmentNumber);

assignmentNumber *=2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);


assignmentNumber /=2;
console.log("Result of division assignment operator: " + assignmentNumber);

/*

	JS follows the PEMDAS rule
	1. 3*4 = 12
	2. 12/5 = 2.4
	3. 1+2 = 3
	4. 3-2.4 = 0.6

*/


let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);


//Increment and Decrement
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied

let z = 1;

let increment = ++z; //pre-increment (no need for reassignment)
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);


increment = z++; //post-increment
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);


// Decrement

let decrement = --z; //pre-increment (no need for reassignment)
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);


decrement = z--; //post-increment
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

// [SECTION] Type Coercion

/*
            - Type coercion is the automatic or implicit conversion of values from one data type to another
            - This happens when operations are performed on different data types that would normally not be possible and yield irregular results
            - Values are automatically converted from one data type to another in order to resolve operations
        */


let numA = '10';
let numB = 12;
let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16, numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);


let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);

// [SECTION] Comparison Operators

// Equality Operator
/* 
    - Checks whether the operands are equal/have the same content
    - Attempts to CONVERT AND COMPARE operands of different data types
    - Returns a boolean value
*/

let juan = 'juan';

console.log(1 == 1); // True
console.log(1 == 2); // False
console.log(1 == '1'); // True
console.log("juan" == "juan");
console.log(juan == "juan");


// Inequality operator (!=)
/* 
            - Checks whether the operands are not equal/have different content
            - Attempts to CONVERT AND COMPARE operands of different data types
        */

console.log(1 != 1);
console.log(1 != 2); 
console.log(1 != '1');
console.log("juan" != "juan");
console.log(juan != "juan");


//Strict Equality Operator (===)

/* 
            - Checks whether the operands are equal/have the same content
            - Also COMPARES the data types of 2 values
            - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
            - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
            - Some programming languages require the developers to explicitly define the data type stored in variables to
- Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
            - Strict equality operators are better to use in most cases to ensure that data types provided are correct
        */


console.log(1 === 1); //t
console.log(1 === 2); //f
console.log(1 === '1'); //t
console.log(0 === false);//f
console.log("juan"=== "juan");//t
console.log(juan === "juan");//t


// Strict Inequality Operator (!==)
/* 
            - Checks whether the operands are not equal/have the same content
            - Also COMPARES the data types of 2 values
*/

console.log(1 !== 2); 
console.log(1 !== 1);
console.log(1 !== '1');
console.log(0 !== false);
console.log("juan" !== "juan");
console.log(juan !== "juan");



//Relational operators
//Some comparison operators check whether one value is greater or less than to the other value.
// has a boolean value

let a = 50;
let b = 65;

// GT Operator (>)
let isGreaterThan = a > b;
//LT Operator (<)
let isLessThan = a < b;
// GTE (>=)
let isGTorEaql = a >= b;
// LT (<=)
let isLTorEqal = a <= b;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEaql);
console.log(isLTorEqal);


console.log(a > '30'); // forced coercion to change the string to a number

console.log(a > 'twenty');

let str = 'forty';
console.log(b >= str); // Non - not a number

// [SECTION] Logical Operators

let isLegalAge = true;
let isRegisterd = false;

// Logical AND operator (&& - double ampersand)
let areAllRequirementsMet = isLegalAge && isRegisterd; 
console.log("Result of logical AND operator: " + areAllRequirementsMet);

//Logical OR operator (|| - Double pipe)
// Return true if one of the operands are true (1+0 = 1)

let someAllRequirementsMet = isLegalAge || isRegisterd; 
console.log("Result of logical OR operator: " + someAllRequirementsMet);

//Logical Not Operator (! - exclamation point)
// Return the opposite value
let areSomeRequirementsNotMet = !isRegisterd;
console.log("Result of logical NOT operator: " + areSomeRequirementsNotMet);