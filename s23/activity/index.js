

let trainer = {

	name: "Rendon",
	age: 30,
	location: "GYM TALILONG",
	talk: function(){
		return "Pikachu! I choose you!"
	}	
};




function Pokemon(name, level){
	// Properties

	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target){

		if (target.health <= 0){

			this.faint(target);
		}

		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + target.health);
	};
	this.faint = function (){
		console.log(this.name + "fainted.");


	}
}

let pikachu = new Pokemon("Pikachu", 10);
let charmander = new Pokemon("Charmander", 7);
let squirtle = new Pokemon("Squirtle", 5);


pikachu.tackle(charmander);



//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}