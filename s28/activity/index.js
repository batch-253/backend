
db.hotel.insertOne({
    "Name": "Single",
    "Accomodates": 2,
    "Price": 1000,
    "description": "A simple room with all the basic necessities",
    "isAvailable": false
});


// INSERT MANY
db.Hotel.insertMany([

        {   
            "Name": "Double",
            "Accomodates": 3,
            "Price": 2000,
            "description": "A room fit for a small family going on vacation",
            "Rooms_Available": 5,
            "isAvailable": false

        },

        {    
            "Name": "Queen",
            "Accomodates": 4,
            "Price": 4000,
            "description": "A room with a queen sized bed perfect for a simple getaway",
            "Rooms_Available": 15,
            "isAvailable": false
        }


    ]);



// FIND DOUBLE
db.Hotel.find({
    "Name": "Double"
});



// UPDATE ONE

db.Hotel.updateOne(

        {
            "name": "Queen"
        },

        {
            $set : { 
            "Rooms_Available": 0
            }
        }
);


//DeleteMany
db.Hotel.deleteMany({

    "Rooms_Available": 0

});

