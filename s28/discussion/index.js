// CRUD Operations
		/*
		    - CRUD operations are the heart of any backend application.
		    - Mastering the CRUD operations is essential for any developer.
		    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
		    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

/*Insert One Document
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			});

		Insert Many Documents
			db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				},
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				}
			]);
*/




// Insert Documents (Create)

db.user.insertOne({
	"firstName": "Mark",
	"lastName": "Ferrer",
	"mobileNumber": "+639123456789",
	"email": "markferrer@mail.com",
	"company": "Zuitt"
});


db.users.insertMany([

	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "8675321",
			email: "stephenhawking@mail.com"
		},
		courses: ["Phyton", "React","PHP","CSS"],
		department: "none"
	},
	{	
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
				phone: "98765432",
				email: "neilarmstrong@mail.com"
			},

		courses: ["React", "Laraverl", "Sass"],
		department: "none"
	}


	])



//Finding Documents (Read/Retrieve)

/*
	Syntax:
        - db.collectionName.find() - find all
        - db.collectionName.find({field: value}); - all document that will match the criteriea
        - db.collectionName.findOne({field: value}) - first document that will mathc the criteria
        - db.collectionName.findOne({}) - find first document
*/

db.users.find();
db.users.find({"firstName": "Stephen"}).pretty();
db.users.findOne({});



db.activity.insertMany ([




	{		
			"name": "Javascript 101"
			"price": 5000,
			"description": "Introduction to Javascript",
			"isActive": true
	},

	{		

			"name": "HTML 101",
			"price": 2000,
			"description": "Introduction to HTML",
			"isActive": true

	}		



]);


//updating/replacing/modifying documents (update)

/*
	Syntax: 
	updating one document
	db.collectionName.updateOne(
		{
			criteria: value
		},
		{
			$set: {
				fieldToBeUpdated: value
			}
		}

	);
		- update the first matching document in our collection

	Multiple/Many Documents
	db.collectionName.updateMany(
		{
			criteria:value
		},
		
		{
	
		{
			$set: {
				fieldToBeUpdated: value
			}
		}

	);

		-update multiple documents that matched the criteria
	

*/


db.user.updateOne(

		{
			"firstName": "Mark"
		},
		{
		$set: {
				"firstName": "Bill",
				"lastName": "Gates",
				"mobileNumber": "123456789",
				"email": "billgates@mail.com",
				"company": "Microsoft",
				"status": "active"
			}	
		}	

);




	{
			"courses":["Phyton","React","PHP","CSS"]

			"courses":["React","Laraverl","Sass"]
		},
		
		{
	
		{
			$set: {
				"courses": value
			}
		}

	);

// Remove Field

db.users.updateOne (
		{
			"firstName": "Bill"
		},
		{
			$unset: {
				"status":active
			}
		}



	);



// Deleting Documents

/*
	Syntax:
		Deleting One Document:
			db.collectionName.deleteOne({"critera": "value"})

		Deleting Multiple Documents:
			db.collectionName.deleteMany({"criteria": "value"})

*/


db.users.deleteOne({
	"company": "Microsoft"
});


db.users.deleteMany({
	"dept": "none"
});



db.users.deleteMany({});






