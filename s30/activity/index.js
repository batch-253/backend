db.fruits.insertMany([
			{
				name : "Apple",
				color : "Red",
				stock : 20,
				price: 40,
				supplier_id : 1,
				onSale : true,
				origin: [ "Philippines", "US" ]
			},

			{
				name : "Banana",
				color : "Yellow",
				stock : 15,
				price: 20,
				supplier_id : 2,
				onSale : true,
				origin: [ "Philippines", "Ecuador" ]
			},

			{
				name : "Kiwi",
				color : "Green",
				stock : 25,
				price: 50,
				supplier_id : 1,
				onSale : true,
				origin: [ "US", "China" ]
			},

			{
				name : "Mango",
				color : "Yellow",
				stock : 10,
				price: 120,
				supplier_id : 2,
				onSale : false,
				origin: [ "Philippines", "India" ]
			}
]);




//2.
db.fruits.count({"onSale": true});

//3.
db.fruits.count({"stock": {
					$gte: 20
}});

//4
db.fruits.aggregate([
		{
			$match:
			{
			"onSale": true
		    }
		},
		{
			$group:
			 {
				"_id": "$supplier_id",  //Grouped by supllier _id
				"total": {$avg: "$price"} // total values of all  "stocks"
			 }
		}
]);

//5
db.fruits.aggregate([
		{
			$match:
			{
			"onSale": true
		    }
		},
		{
			$group:
			 {
				"_id": "$supplier_id",  //Grouped by supllier _id
				"total": {$max: "$price"} // total values of all  "stocks"
			 }
		}
]);

//6
db.fruits.aggregate([
		{
			$match:
			{
			"onSale": true
		    }
		},
		{
			$group:
			 {
				"_id": "$supplier_id",  //Grouped by supllier _id
				"total": {$min: "$price"} // total values of all  "stocks"
			 }
		}
]);

