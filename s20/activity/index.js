console.log("Hello World");


function printNumbers(num) {
  console.log("Number provided: " + num);

  for (let i = num; i >= 0; i--) {
    if (i <= 50) {
      console.log("The current value is at " + i + ". Terminating the loop.");
      break;
    } else if (i % 10 === 0) {
      console.log("The number is divisible by 10. Skipping the number.");
      continue;
    } else if (i % 5 === 0) {
      console.log(i);
    }
  }
}

//printNumbers(100);



//Objective 2

let string = 'supercalifragilisticexpialidocious';

let filteredString = '';

console.log(string);

for (let i = 0; i < string.length; i++) {

  let char = string[i].toLowerCase();

  if (char !== 'a' && char !== 'e' && char !== 'i' && char !== 'o' && char !== 'u') 

  { filteredString += string[i]; }
}

console.log(filteredString);


//Do not modify
//For exporting to test.js
try {
    module.exports = {
       printNumbers, filteredString
    }
} catch(err) {

}