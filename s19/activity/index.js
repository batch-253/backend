console.log("Hello World");

function login(username, password, role) {
  if (!username || !password || !role) {
    return "Your input should not be empty.";
  } else {
    switch (role.toLowerCase()) {
      case "admin":
        return "Welcome back to the class portal, admin!";
        break;
      case "teacher":
        return "Thank you for logging in, teacher!";
        break;
      case "student":
        return "Welcome to the class portal, student!";
        break;
      default:
        return "Role out of range.";
    }
  }
}

// login("adminboss123", "pass123", "admin");  // "Welcome back to the class portal, admin!"
// login("marky", "pass123", "teacher"); // "Thank you for logging in, teacher!"
// login("makoy", "pass123", "student");  // "Welcome to the class portal, student!"
// login("", "pass123", "student");         // "Your input should not be empty."
// login("mmekky", "pass123", "vicepresident"); // "Role out of range."

function checkAverage(num1, num2, num3, num4) {
  let average = (num1 + num2 + num3 + num4) / 4;
  average = Math.round(average);
  console.log(average);

  if (average <= 74) {
    return (
      "Hello, student, your average is." +
      average +
      "The letter equivalent is F"
    );
  } else if (average >= 75 && average <= 79) {
    return (
      "Hello, student, your average is" + average + "The letter equivalent is D"
    );
  } else if (average >= 80 && average <= 84) {
    return (
      "Hello, student, your average is" + average + "The letter equivalent is C"
    );
  } else if (average >= 85 && average <= 89) {
    return (
      "Hello, student, your average is" + average + "The letter equivalent is B"
    );
  } else if (average >= 90 && average <= 95) {
    return (
      "Hello, student, your average is" + average + "The letter equivalent is A"
    );
  } else if (average >= 96) {
    return (
      "Hello, student, your average is" +
      average +
      "The letter equivalent is A+"
    );
  }
}

result = checkAverage(71, 70, 73, 71);
console.log(result);

result1 = checkAverage(75, 75, 76, 78);
console.log(result1);

result2 = checkAverage(80, 82, 83, 81);
console.log(result2);

result3 = checkAverage(85, 86, 85, 86);
console.log(result3);

result4 = checkAverage(91, 90, 92, 90);
console.log(result4);

result5 = checkAverage(95, 96, 97, 96);
console.log(result5);

function checkGift(day) {
  let gifts = ["pear tree", "turtle doves", "french hens", "golden rings"];

  if (day > 0 && day < 4) {
    return `i was given ${day} ${gifts[day - 1]}`;
  } else {
    return "no gifts were given";
  }
}

let row = 0;
let col = 0;

for (let row = 1; row < 3; row++) {
  for (let col = 1; col <= row; col++) {
    console.log(`Current row:${row}, current col: ${col}`);
  }
}

function checkDivisibility(dividend, divisor) {
  if (dividend % divisor == 0) {
    console.log(` 1 ${dividend} ${divisor}`);
  } else {
    console.log(` 2 ${dividend} ${divisor}`);
  }
}

for (let i = 1; i < 5; i++) {
  console.log(i * i);
}

let students = [`j,p,g,r`];

console.log(`grad`);

for (let count = 0; count <= students.length; count++) {
  console.log(students[count]);
}
