console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	
	
	
	let firstName = "Mark Anthony";
	console.log("My First Name: " + firstName);

	let lastName = "Ferrer";
	console.log("My Last Name: " + lastName);

	let age = 30;
	console.log("My age is: " + age);

	let hobbies = ['biking', 'mountain climbing', 'Swimming'];
	console.log("Hobbies:");
	console.log(hobbies)

	let workAddress = {
		houseNumber: 32,
		street: "washington",
		city: "lincoln",
		state: "Nebraska"};

	console.log("Work Addres:");	
	console.log(workAddress);

	let fullName = "Steve Rogers"
	console.log("My Full Name: " + fullName)

	let currentAge = 30;
	console.log("My current age is: " + currentAge);

	let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
	console.log("My Friends are: ")
	console.log(friends);
	
	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: " + profile);
	console.log(profile);

	let fullName2 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName2);

	let lastLocation = "Arctic Ocean";
	
	console.log("I was found frozen in: " + lastLocation);

	try{
        module.exports = {
            firstName, lastName, age, hobbies, workAddress,
            fullName, currentAge, friends, profile, fullName2, lastLocation
        }
    } catch(err){
    }